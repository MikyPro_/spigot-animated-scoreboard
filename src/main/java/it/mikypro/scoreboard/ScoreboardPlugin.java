package it.mikypro.scoreboard;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.*;

import java.util.Random;

/*
* Class created by MikyPro_ on 09/02/2019
*/

public final class ScoreboardPlugin extends JavaPlugin implements Listener {

    private Scoreboard scoreboard;

    @Override
    public void onEnable() {
        // Plugin startup logic
        ScoreboardManager manager = getServer().getScoreboardManager();
        scoreboard = manager.getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("scoreboard", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.getScore("§aABC").setScore(0);
        objective.setDisplayName("§cDEF");

        new BukkitRunnable() {
            @Override
            public void run() {
                objective.setDisplayName("§" + new Random().nextInt(9) + "DEF");
            }
        }.runTaskTimerAsynchronously(this, 0, 5);

        getServer().getPluginManager().registerEvents(this,this);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        event.getPlayer().setScoreboard(scoreboard);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
